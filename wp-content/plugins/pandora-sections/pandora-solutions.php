<?php
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
add_action( 'init', 'create_pandora_solutions' );

function create_pandora_solutions() {
    register_post_type( 'solutions',
        array(
            'labels' => array(
                'name' => 'PB Solutions',
                'singular_name' => 'PB Solution',
                'add_new' => 'Add New',
                'add_new_item' => 'Add New Solution',
                'edit' => 'Edit',
                'edit_item' => 'Edit Solution',
                'new_item' => 'New Solution',
                'view' => 'View',
                'view_item' => 'View Solution',
                'search_items' => 'Search Solutions',
                'not_found' => 'No solutions found',
                'not_found_in_trash' => 'No solutions found in Trash',
                'parent' => 'Parent solution'
            ),
            'public' => true,
            'menu_position' => 111,
            'supports' => array( 'title', 'editor' ,'thumbnail' ),
            'taxonomies' => array( '' ),   
            'has_archive' => true
        )
    );
}