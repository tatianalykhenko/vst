<?php
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
add_action( 'init', 'create_pandora_producttypes' );

function create_pandora_producttypes() {
    register_post_type( 'product-categories',
        array(
            'labels' => array(
                'name' => 'PB Product Types',
                'singular_name' => 'PB Product Type',
                'add_new' => 'Add New',
                'add_new_item' => 'Add New Product Type',
                'edit' => 'Edit',
                'edit_item' => 'Edit Product Type',
                'new_item' => 'New Product Type',
                'view' => 'View',
                'view_item' => 'View Product Type',
                'search_items' => 'Search Product Types',
                'not_found' => 'No product Types found',
                'not_found_in_trash' => 'No product Types found in Trash',
                'parent' => 'Parent product Type'
            ),
            'public' => true,
            'menu_position' => 111,
            'supports' => array( 'title', 'editor' ,'thumbnail' ),
            'taxonomies' => array( 'category' ),   
            'has_archive' => true,
            'rewrite' => array(
                'slug' => 'products-by-category'
            )
        )
    );
}

?>