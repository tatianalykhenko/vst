<?php
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
add_action( 'init', 'create_pandora_products' );

function create_pandora_products() {
    register_post_type( 'products',
        array(
            'labels' => array(
                'name' => 'PB Products',
                'singular_name' => 'PB Product',
                'add_new' => 'Add New',
                'add_new_item' => 'Add New Product',
                'edit' => 'Edit',
                'edit_item' => 'Edit Product',
                'new_item' => 'New Product',
                'view' => 'View',
                'view_item' => 'View Product',
                'search_items' => 'Search Products',
                'not_found' => 'No products found',
                'not_found_in_trash' => 'No products found in Trash',
                'parent' => 'Parent product'
            ),
            'public' => true,
            'menu_position' => 111,
            'supports' => array( 'title', 'editor' ,'thumbnail' ),
            'taxonomies' => array( '' ),   
            'has_archive' => true
        )
    );
}

add_action( 'save_post', 'add_product_fields', 0);

function add_product_fields($post_id) 
{     
    if (isset($_POST['product_fields_nonce']) && !wp_verify_nonce($_POST['product_fields_nonce'], __FILE__) ) return false; 
    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE  ) return false;  
    if ( !current_user_can('edit_post', $post_id) ) return false;  
    
    if (isset($_POST['product_icon_select'])) update_post_meta($post_id, 'product_icon', esc_attr( $_POST['product_icon_select'] )); 
    if (isset($_POST['product_link'])) update_post_meta($post_id, 'product_link', stripcslashes($_POST['product_link'])); 

    return $post_id;  
} 

add_action( 'admin_init', 'products_icon_select', 1);
add_action( 'admin_init', 'products_link', 1);
add_action('do_meta_boxes', 'remove_fimage_box');

if ( ! function_exists('remove_fimage_box')) {
    function remove_fimage_box(){
        remove_meta_box( 'postimagediv', 'pandora-products', 'side' );
    }
}


function products_icon_select() {
    add_meta_box( 'product_icon_select',
        'Select icon',
        'product_icon_select_meta_box',
        'pandora-products', 'normal', 'high'
    );
}

function products_link() {
    add_meta_box( 'product_link',
        'Add link',
        'product_link_meta_box',
        'pandora-products', 'normal', 'high'
    );
}

function product_icon_select_meta_box( $post ) {  
    $product_icon = get_post_meta($post->ID, 'product_icon');
    ?>

    <?php if (is_plugin_active('font-awesome/plugin.php')) {?>
    <table>
        <tr>
            <th style="min-width: 70px;">product icon:</th>
            <td style="font-size: 18px; text-align: center; color: #777;">
                <i title="Old icon" class="<?php echo $product_icon[0] ?>"></i>
            </td>
            <td>
                <select style="width: 200px; font-family:'FontAwesome', Arial; font-size: 14px; color: #333;" name="product_icon_select">
                        <option value="<?php if (isset($product_icon[0])) echo $product_icon[0]; ?>"> <?php if (isset($product_icon[0])) echo $product_icon[0]; ?></option> 
                        <option> </option>
                        <?php readfile(plugin_dir_path( __FILE__ ) . "icons-list.html"); ?>
                </select> 
            </td>
        </tr>
    </table>
    <?php } else {?>
        <blockquote>Plugin <a href="http://wordpress.org/plugins/font-awesome/"> FontAwesome Icons</a> don't activated! Please, install and enable it on <a href="<?php echo esc_url(home_url()) . '/wp-admin/plugins.php' ?>" >Plugins</a> page.</blockquote>
      <?php } ?>

    <input type="hidden" name="product_fields_nonce" value="<?php echo wp_create_nonce(__FILE__); ?>" />  
<?php
}


function product_link_meta_box( $post ) {  
    $product_link = get_post_meta($post->ID, 'product_link', true);
    ?>

    <table style="width: 100%;">
        <tr>
            <th style="width: 60px;">Link:</th>
            <td>
            <input type="text" name="product_link" style="width: 100%;" value="<?php if (isset($product_link)) echo $product_link; ?>">
            </td>
        </tr>
    </table>

<?php } ?>