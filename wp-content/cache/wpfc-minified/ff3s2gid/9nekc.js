// source --> https://www.vstsecuritysolutions.com/wp-content/themes/PandoraBox/js/map.js?ver=1.3.7 
var mapAddress, markerTitle;

function initialize() {
	var geocoder;
	var map;

  	geocoder = new google.maps.Geocoder();
  	var latlng = new google.maps.LatLng(41.5514, -87.3600);
  	var mapOptions = {
	    zoom: 12,
      scrollwheel: false,
	    center: latlng,
	    mapTypeId: google.maps.MapTypeId.ROADMAP
	}

  map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

  geocoder.geocode( { 'address': mapAddress}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      map.setCenter(latlng);
      map.panBy(200,0);
      var marker = new google.maps.Marker({
          map: map,
          position: results[0].geometry.location,
          title: markerTitle
      });
    } else {
      //alert('Geocode was not successful for the following reason: ' + status);
    }
  });
}

function getMap(address, mtitle){
if (address) {mapAddress = address} else {mapAddress = 'Munster IN USA'}
markerTitle = mtitle;
google.maps.event.addDomListener(window, 'load', initialize);
//google.maps.event.trigger(map, 'resize');
};