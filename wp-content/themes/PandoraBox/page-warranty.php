<?php
/*
 * Template Name: Warranty
 */
?>

<?php
get_header();
$advanced_custom_fields = get_fields($post->ID);
?>
<div
    class="block colorblock"
    id="">
    <div class="block-container">
        <h1 class="title"><?php the_title(); ?></h1>
        <div class="description"><?php echo $advanced_custom_fields['tag_line']; ?></div>
        <div class="content">
            <div class="hexagon">
                <div class="outer"></div>
                <div class="inner"></div>
            </div>
        </div>
    </div>
</div>

<div class="block whiteblock blogblock">
    <div
        id="blog"
        class="block-container">
        <div class="table">
            <div class="postlist">
            <?php while ( have_posts() ) : the_post(); ?>
            <article
                    id="post-<?php the_ID(); ?>"
                    <?php post_class('single-page'); ?>>
                    <div style="text-align: center"> <?php the_post_thumbnail(); ?> </div>
                    <div class="post-body">
                        <?php the_content(); ?>
                    </div>
                </article>
            <?php endwhile; ?>

            </div>
            <?php if($advanced_custom_fields['sidebar_content']): ?>
            <aside class="sidebar" style="top:40px;">
                <?php echo $advanced_custom_fields['sidebar_content']; ?>
            </aside>
            <?php endif; ?>
        </div>
    </div>
</div>

<script>
jQuery(document).ready(function(){
	jQuery('.block .content').prepend( '<div class="hexagon"><div class="outer"></div><div class="inner"></div></div>');
});
</script>

<?php get_footer(); ?>