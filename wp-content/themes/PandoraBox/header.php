<?php 
    $pandora_options = get_option('pandora_options'); 
    $menu_icons = get_option('menu_icons');
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>> 
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo('charset'); ?>">
    <title><?php  wp_title(); ?></title>
    <title><?php // wp_description(); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-201378961-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-201378961-1');
</script>


    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <div class="iconmenu">
        <i class="fas fa-bars"></i>
    </div>
    <div class="mainmenu">
        <div class="menuwrapper">
            <nav class="menulist <?php if (isset($pandora_options['menu_position']) && $pandora_options['menu_position'] == "right") echo 'menu-right' ?>">
                <?php 
                    wp_nav_menu (array(
                        'theme_location'    => 'primary',
                        'container'         =>'',
                        'container_class'   => '',
                        'container_id'      => '',
                        'sort_column'       => 'menu_order',
                        'menu_class'        => 'items',
                        'fallback_cb'       => '',
                        'items_wrap'        => '<ul class="items">%3$s</ul>', 
                        'walker'            => ''
                        ));
                ?>
            </nav>
            <a href="<?php echo esc_url(home_url()); ?>" class="logoblock">
            <?php if (isset($pandora_options['logo_image']) && $pandora_options['logo_image'] != "") { ?>
            	<img class="companylogo" src="<?php echo $pandora_options['logo_image']; ?>" alt="VST Security Solutions logo" title="VST Security Solutions logo">
            <?php } else { ?>
                	<span class="companyname"><?php bloginfo('name'); ?></span>
            <?php } ?>
            </a>

            <?php if (empty($pandora_options['menu_position']) || $pandora_options['menu_position'] != "right") {?>
            <div class="socialblock">
                <?php for ($i=0; $i < count($menu_icons); $i++){ ?>
                <a href="<?php echo $menu_icons[$i]['menu_icon_link'];?>" title="<?php echo $menu_icons[$i]['menu_icon_alt'];?>" class="socialicon"><i class="<?php echo $menu_icons[$i]['menu_icon'];?>"></i></a>
                <?php } ?>
            </div>
            <?php } ?>
        </div>
    </div>