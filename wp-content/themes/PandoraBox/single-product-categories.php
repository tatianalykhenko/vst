

<?php
get_header();

$filter[] = array(
    'key' => 'product_types',
    'value' => $post->ID,
    'compare' => 'like',
);
$products = new WP_Query( array( 'post_type' => 'products', 'meta_query' => $filter ) );
wp_reset_query();

$advanced_custom_fields = get_fields($post->ID);
?>
<div
    class="block colorblock"
    id="">
    <div class="block-container">
        <h1 class="title" style="font-size:225%;"><?php echo $post->post_title; ?> Products</h1>
        <div class="description"><?php echo $advanced_custom_fields['tag_line']; ?></div>
        <div class="content">
            <div class="hexagon">
                <div class="outer"></div>
                <div class="inner"></div>
            </div>
        </div>
    </div>
</div>



<div class="block whiteblock blogblock">
    <div
        id="blog"
        class="block-container">
        
        <div class="table">
            <div class="postlist">
            <?php 
            if($products->posts):             
            foreach($products->posts as $key => $product): 
            
            if($key == 0 || $key%2 == 0) {
                $float = 'left';
                $align = 'left';
            } else {
                $float = 'left';
                $align = 'left';
            }
            
            // advanced custom fields
            $advanced_custom_fields = get_fields($product->ID);

            ?>
            <article>


                    <div class="slide-wrapper mb-50 mt-50">
                        <div class="picture" style="float:<?php echo $float; ?>;width:15%;">
                            <img
                                src="<?php echo $advanced_custom_fields['images'][0]['url']; ?>"
                                class="attachment-large wp-post-image">
                        </div>
                        <div class="container" style="float:<?php echo $align; ?>;width:30%;padding:20px;">
                            <h2 class="title" style="text-align:<?php echo $align; ?>;"><?php echo $product->post_title; ?></h2>
                            <div class="description" style="padding:0;text-align:<?php echo $align; ?>;font-size:16px;">
                                <p>
                                    <?php echo $product->post_content; ?>
                                    <?php 
                                    if($advanced_custom_fields['features']): ?>
                                    <h4>Features: </h4>
                                    <ul>
                                        <?php foreach($advanced_custom_fields['features'] as $feature): ?>
                                            <li><?php echo $feature['feature']; ?></li>
                                        <?php endforeach; ?>
                                    </ul>
                                    <?php endif; ?>
                                    <a href="<?php echo get_permalink(171); ?>#contacts" class="outlined-button inverse">Contact us about this item</a>
                                </p>
                            </div>
                        </div>
                    </div>

                </article>
            <?php endforeach;endif; ?>

            </div>
            <?php if($advanced_custom_fields['sidebar_content']): ?>
            <aside class="sidebar" style="top:40px;">
                <?php echo $advanced_custom_fields['sidebar_content']; ?>
            </aside>
            <?php endif; ?>
        </div>
    </div>
</div>

<script>
jQuery(document).ready(function(){
	jQuery('.block .content').prepend( '<div class="hexagon"><div class="outer"></div><div class="inner"></div></div>');
});
</script>

<?php get_footer(); ?>