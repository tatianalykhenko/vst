<?php
/*
 * Template Name: Solutions
 */
?>

<?php
get_header();
$advanced_custom_fields = get_fields($post->ID);
?>
<div
    class="block colorblock"
    id="">
    <div class="block-container">
        <h1 class="title"><?php the_title(); ?></h1>
        <div class="description"><?php echo $advanced_custom_fields['tag_line']; ?></div>
        <div class="content">
            <div class="hexagon">
                <div class="outer"></div>
                <div class="inner"></div>
            </div>
        </div>
    </div>
</div>


<div class="block whiteblock blogblock">
    <div
        id="blog"
        class="block-container">
        <div class="table">
            <div class="postlist">
            <?php 
            $products = new WP_Query( array( 'post_type' => 'solutions' ));
            wp_reset_query();
            ?>
            <?php 
            if($products->posts):             
            foreach($products->posts as $key => $product): 
            
            if($key == 0 || $key%2 == 0) {
                $float = 'left';
                $align = 'right';
            } else {
                $float = 'right';
                $align = 'left';
            }
            
            // advanced custom fields
            $product_advanced_custom_fields = get_fields($product->ID);

            ?>
            <article >
                    
                    
                    <div class="slide-wrapper mb-50 mt-50">
                        <div class="picture" style="float:<?php echo $float; ?>;text-align:<?php echo $float;?>;width:25%;">
                            <img
                                src="<?php echo $product_advanced_custom_fields['images'][0]['url']; ?>"
                                alt="<?php echo $product_advanced_custom_fields['images'][0]['alt']; ?>" 
                                class="attachment-large wp-post-image"
                                style="max-height:300px;" >
                        </div>
                        <div class="container" style="float:<?php echo $align; ?>;width:70%;">
                            <h2 class="title" style="text-align:<?php echo $align; ?>;"><?php echo $product->post_title; ?></h2>
                            <div class="description" style="padding:0;text-align:<?php echo $align; ?>;font-size:16px;">
                                <p>
                                    <?php echo $product->post_content; ?>
                                </p>
                                    <?php 
                                    if($product_advanced_custom_fields['product_types']):
                                        foreach($product_advanced_custom_fields['product_types'] as $prod_type): 
                                    ?>
                                    <a href="<?php echo get_permalink($prod_type->ID); ?>" class="outlined-button">View Our <?php echo $prod_type->post_title; ?> Products</a><br><br>
                                    <?php endforeach; endif; ?>
                                    
                                    <a href="<?php echo get_permalink(171); ?>#contacts" class="outlined-button inverse">Contact us about this service</a>
                            </div>
                        </div>
                    </div>
                    
                    <div class="clear"></div>
                    <hr class="product-sep" >
                    
                </article>
            <?php endforeach;endif; ?>

            </div>
            <?php if($advanced_custom_fields['sidebar_content']): ?>
            <aside class="sidebar" style="top:40px;">
                <?php echo $advanced_custom_fields['sidebar_content']; ?>
            </aside>
            <?php endif; ?>
        </div>
    </div>
</div>

<script>
jQuery(document).ready(function(){
	jQuery('.block .content').prepend( '<div class="hexagon"><div class="outer"></div><div class="inner"></div></div>');
});
</script>

<?php get_footer(); ?>