<?php 
    $pandora_options = get_option('pandora_options');
    $home_store = get_option('home_store_icons');
    $advanced_custom_fields = get_fields($post->ID);

?>
    <div id="page-hero" class="homeblock colorblock block">
        <div  class="block-container">
      
            <div id="description-home">
                <div class="wrapper">
                    <h1 id="header" class="maintext"><?php echo $advanced_custom_fields['hero_title'];?></h1>
                    <?php echo $advanced_custom_fields['hero_subtitles'];?>
                </div>
            </div>
            <div id="picture-home">
                <div class="wrapper">
                    <?php if($pandora_options['home_image_second']) { ?>
                    <div class="tablet container">
                        <div class="stack">
                            <img class="gadget" src="<?php echo $pandora_options['home_image_second'];?>" alt="Video surveillance system">
                        </div>
                        <div class="shadow"></div>
                    </div>
                    <?php } ?>

                    <?php if($pandora_options['home_image_first']) { ?>
                    <div class="phone container">
                        <div class="stack">
                            <img class="gadget" src="<?php echo $pandora_options['home_image_first'];?>" alt="Video surveillance system">
                        </div>
                        <div class="shadow"></div>
                    </div>
                    <?php } ?>
                </div>
            </div>

            <div class="hexagon">
                <div class="outer"></div>
                <div class="inner"></div>
            </div>
        </div>
    </div>
    