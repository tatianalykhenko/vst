<?php
/*
 * Template Name: About 
 */
?>

<?php
get_header();
$advanced_custom_fields = get_fields($post->ID);
?>
<div
    class="block colorblock"
    id="">
    <div class="block-container">
        <h1 class="title"><?php the_title(); ?></h1>
        <div class="description"><?php echo $advanced_custom_fields['tag_line']; ?></div>
        <div class="content">
            <div class="hexagon">
                <div class="outer"></div>
                <div class="inner"></div>
            </div>
        </div>
    </div>
</div>

<div class="block whiteblock blogblock">
    <div
        id="blog"
        class="block-container">
        <div class="table">
            <div class="postlist">
            <?php while ( have_posts() ) : the_post(); ?>
            <article
                    id="post-<?php the_ID(); ?>"
                    <?php post_class('single-page'); ?>>
                    <div style="text-align: center"> <?php the_post_thumbnail(); ?> </div>
                    <div class="post-body">
                        <?php the_content(); ?>
                    </div>
                </article>
            <?php endwhile; ?>

            </div>
            <aside class="sidebar">
            
            <?php $pandora_options = get_option('pandora_options'); ?>
<div class="teamblock whiteblock block">
                    <div
                        id="team"
                        class="block-container"
                        style="text-align: center;">
        <?php if (!empty($pandora_options['team_title'])) { ?> <h2 class="title"><?php echo $pandora_options['team_title']; ?></h2> <?php } ?>
        <?php if (!empty($pandora_options['team_subtitle'])) { ?> <div class="description"><?php echo $pandora_options['team_subtitle']; ?></div> <?php } ?>           
        <div
                            class="teamcontainer"
                            style="margin-top: 30px;">
            
            <?php query_posts(array('post_type'=>'pandora-team', 'posts_per_page'=>-1)); ?>       
            <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?> 
            <div class="personal">
                <?php
                $advanced_custom_fields = get_fields($post->ID);
                $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
                $team_photo_link = get_post_meta($post->ID, 'team_photo_link', true);
                if ($team_photo_link) {echo '<a href="'.$team_photo_link.'">'; }
                ?>
                    <div class="photo" style="display:none;background-image: url('<?php echo $url ?>')"></div>
                <?php if ($team_photo_link) {echo '</a>';} ?>
                
                <div class="userinfo" style="margin-top:0;">
                                    <div class="name"><?php the_title(); ?></div>
                                    <div
                                        class="status"
                                        style=""><?php echo (get_post_meta($post->ID, 'profession', true)); ?></div>
                                    <div
                                        class="status"
                                        style="margin-top: 10px; font-size: 14px;"><?php echo $advanced_custom_fields['about']; ?></div>

                    </div>
                                </div>
                            </div>
                            <hr style="margin-bottom: 40px;">
            <?php endwhile; ?>

        </div>
                    </div>
                </div>
            </aside>
        </div>
    </div>
</div>

<script>
jQuery(document).ready(function(){
	jQuery('.block .content').prepend( '<div class="hexagon"><div class="outer"></div><div class="inner"></div></div>');
});
</script>

<?php get_footer(); ?>
