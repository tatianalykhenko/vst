<?php
/*
 * Template Name: Commercial Video Surveillance
 */
?>

<?php
get_header();
$advanced_custom_fields = get_fields($post->ID);
?>
<?php get_template_part('section', 'video-hero'); ?>

<div class="teamblock whiteblock block">
    <div id="what-we-do" class="block-container text-left">
        <h2><?php echo $advanced_custom_fields['what_we_do_title']; ?></h2>
        <?php echo $advanced_custom_fields['what_we_do_description']; ?>
        <div class="mb-45"></div>
        <h2><?php echo $advanced_custom_fields['video_equipment_title']; ?></h2>
        <?php echo $advanced_custom_fields['video_equipment_description']; ?>
        <div class="apps">
            <?php if (is_plugin_active('pandora-slider/pandora-slider.php')) echo do_shortcode('[pb_slider]'); ?>
        </div>
    </div>
</div>

<?php echo do_shortcode('[pb_block id="401"]'); ?>

<div class="teamblock whiteblock block">
    <div id="benefits" class="block-container text-left">
        <h2><?php echo $advanced_custom_fields['benefits_title']; ?></h2>

        <?php echo $advanced_custom_fields['benefits_description']; ?>
        
        <div class="mb-45"></div>
        
        <h2><?php echo $advanced_custom_fields['faqs_title']; ?></h2>

        <?php
        
        $faqs = $advanced_custom_fields['faqs'];

        if( ($faqs) ) {
            foreach ($faqs as $faq) {
                echo '<h4>' . $faq['question'] . '</h4>';
                echo $faq['answer'];
            }
        }
        ?>
        

    </div>
</div>

<?php echo do_shortcode('[pb_block id="402"]'); ?>

<?php get_template_part('section', 'contact'); ?>

<?php get_footer(); ?>
